import { useEffect } from "react";
import { useCreateCurrencyMutation, useGetCurrenciesQuery, useUpdateCurrencyMutation } from "./store/currencies/currenciesApiSlice"


function App() {

  const {data, isSuccess} = useGetCurrenciesQuery()
  const [createCurrency] = useCreateCurrencyMutation()

  const [updateCurrency] = useUpdateCurrencyMutation()

  async function createNewCurrency(e){
    try{

      const response = await createCurrency({id: "20", name: "GBP", symbol: "$", logo: ""})
    }catch(e){
      console.log(e);
    }
  }
  console.log(data) // 1,2,3,4,5,6


  return (
   <>
    <button onClick={e => refetch()}>Actualizar</button>
    {
      isSuccess && data.map((d, index) =>{
        return (
          <div>{d.abbreviation} {new Date().toLocaleString()}</div>
        )
      })
    }
    <button onClick={e => createNewCurrency()}>Crear divisa</button>
   </>
  )
}

export default App

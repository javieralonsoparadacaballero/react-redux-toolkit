import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const baseURL = "http://127.0.0.1:8000/api/"

export const apiSlice = createApi({
  reducerPath: "api",
  baseQuery: fetchBaseQuery({
    baseUrl: baseURL,
    tagTypes: ["Currencies"],
    prepareHeaders: async (headers, { getState }) => {
      // Retrieve the auth token from the state
      let token = localStorage.getItem('access')
      let refresh = localStorage.getItem('refresh')
      // If we have a token, set the authorization header
      if (token) {
        const timestamp = localStorage.getItem('timestamp')
        const now = (new Date().getTime())
        const expiry = parseInt(timestamp)
        if (expiry < now) {
          try {
            let response = await fetch(baseURL + 'auth/token/refresh/', {
              method: 'POST',
              body: JSON.stringify({
                'refresh': refresh
              }),
              headers: { 'Content-Type': 'application/json' }
            })
            response = await response.json()
            token = response.access;
            localStorage.setItem("access", response.access);
            localStorage.setItem("refresh", response.refresh);
            console.log(now + (5 * 60 * 1000));
            console.log(expiry);
            localStorage.setItem("timestamp", now + (5 * 60 * 1000));
          } catch (error) {
            // Si hay algún error al obtener el nuevo token, eliminamos los tokens de sesión
            localStorage.removeItem("access");
            localStorage.removeItem("refresh");
            localStorage.removeItem("timestamp");
          }
        }
        headers.set('Authorization', `Bearer ${token}`);
      }

      return headers;
    },
  }),
  endpoints: (builder) => ({}),
});

import { apiSlice } from "../apiSlice";

const currenciesURL = 'get-divisas/'

export const currenciesApi = apiSlice.injectEndpoints({
    endpoints: (builder) => ({
        getCurrencies: builder.query({
            query: () => ({
                url: currenciesURL,
            }),
            providesTags: ['Currencies', 'Reservations']
        }),
        createCurrency: builder.mutation({
            query: (data) => ({
                url: currenciesURL,
                method: "POST",
                body: data
            }),
            invalidatesTags: ['Reservations']
        }),
        updateCurrency: builder.mutation({
            query: (data) => ({
                url: currenciesURL+`${data.id}/`,
                method: "PATCH",
                body: data
            }),
            invalidatesTags: ['Currencies']
        })
    }),
});
export const { useGetCurrenciesQuery, useCreateCurrencyMutation, useUpdateCurrencyMutation } = currenciesApi;
